import http from "node:http";

(async () => {
  let handler, method;

  await import("./handler.mjs")
    .catch(err => {
      console.log("[INFO] An error occurred when reading handler.mjs");
      console.log(err);
      return import("./index.js");
    })
    .then(fun => {
      handler = fun.handler;
      method = fun.method;
    });

  const port = 3000;

  const server = http.createServer();

  server.on("request", async (req, res) => {
    if (!method)
      console.warn("No method defined !");

    if (method && req.method != method) {
      res.setHeader("Allow", method);
      res.statusCode = 405;
      res.end();

      return;
    }

    const buffers = [];

    for await (const chunk of req) {
      buffers.push(chunk);
    }

    const data = Buffer.concat(buffers).toString();

    const event = {
      body: data,
      headers: req.headers,
      method: req.method,
      path: req.url,
    };

    handler(event).then(response => {
      res.statusCode = response.statusCode;

      if (response.headers) {
        Object.entries(response.headers).forEach(([header, value]) => res.setHeader(header, value));
      }

      if (response.ttl) {
        res.setHeader("Cache-Control", `public, max-age=${response.ttl}`);
        res.setHeader("Age", "0");
      }

      if (response.stream) {
        response.stream.pipe(res);
      } else {
        res.end(response.body);
      }
    });
  });

  server.listen(port);
}) ();
