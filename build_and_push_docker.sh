#!/bin/bash

set -euo pipefail

docker login registry.gitlab.com

cd faasd-server
docker buildx build -t registry.gitlab.com/vnourdin/faasd-server/faasd-server --load .
docker push registry.gitlab.com/vnourdin/faasd-server/faasd-server
cd ..

cd docker-ci-image
docker buildx build -t registry.gitlab.com/vnourdin/faasd-server/docker-ci-image --load .
docker push registry.gitlab.com/vnourdin/faasd-server/docker-ci-image
cd ..

cd rsync-ci-image
docker buildx build -t registry.gitlab.com/vnourdin/faasd-server/rsync-ci-image --load .
docker push registry.gitlab.com/vnourdin/faasd-server/rsync-ci-image
cd ..
